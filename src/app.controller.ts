import { Body, Controller, Get, Post } from '@nestjs/common';
import { BlockChainBlocksResponse, BlockChainMineResponseInterface } from './blockChain/block-chain.interface';
import { BlockChainService } from './blockChain/block-chain.service';
import { P2pService } from './p2p/p2p.service';

@Controller()
export class AppController {

  constructor(private readonly blockchain: BlockChainService,
    private readonly p2pService: P2pService
  ) {}

  @Get('blocks')
  blockChain(): BlockChainBlocksResponse {
    return this.blockchain.blocks;
  }

  @Post('mine')
  addBlock(@Body() body): BlockChainMineResponseInterface {
    const dataString = JSON.stringify(body);
    const response: BlockChainMineResponseInterface = this.blockchain.add(dataString);
    
    this.p2pService.sync();
    
    return response;

  }

  @Get('blockChainValidate')
  blockChainValidate(): Boolean {
    return this.blockchain.validate;
  }
}
