export interface BlockInterface {
    timestamp: number;
    nonce: number;
    previousHash: string;
    hash: string;
    data: any;
    difficulty: number;
}