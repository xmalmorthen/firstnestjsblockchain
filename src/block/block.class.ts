import { BlockInterface } from "./block.interface";
import { SHA256 } from 'crypto-js';
import { DifficultyClass } from "./difficulty.class";

export class BlockClass {
    public block: BlockInterface = null;

    private static difficulty: number = 3;

    constructor( block: BlockInterface ){
        this.block = block;
    }

    static calculateHash(previousBlock: BlockInterface, data: any): { timestamp: number, nonce: number, hash: string, difficulty: number }{
        let timestamp: number;
        let hash: string;
        let nonce: number = 0;
        let difficulty: number = previousBlock ? previousBlock.difficulty : this.difficulty;
        do{
            timestamp = Date.now();
            nonce += 1;
            difficulty =  previousBlock ? DifficultyClass.calculate(previousBlock,timestamp) : this.difficulty;
            hash = this.hash(timestamp,nonce, previousBlock?.previousHash || null, data, difficulty);
        } while (hash.substring(0,difficulty) !== '0'.repeat(difficulty))

        return { timestamp, nonce, hash, difficulty};
    }

    static get genesis(): BlockClass{
        const data: string = 'genesis block';
        const {timestamp, nonce, hash, difficulty} = BlockClass.calculateHash(null,data);
        return new this( { timestamp, nonce, previousHash: null, hash, data, difficulty } );
    }

    static mine(previousBlock: BlockInterface, data: any){
        
        const {timestamp,hash, difficulty, nonce} = BlockClass.calculateHash(previousBlock,data);

        return new this( { 
            timestamp,
            nonce,
            previousHash: previousBlock.hash, 
            hash,
            data,
            difficulty
        } );
    }

    public static hash(timestamp: number, nonce: number, previousHash: string, data: any, difficulty: number): string{
        return SHA256(`${timestamp}${nonce}${previousHash}${data}${difficulty}`).toString();
    }

    toString(): string {
        return `Block [ timestamp: ${this.block.timestamp}, nonce: ${this.block.nonce}, previousHash: ${this.block.previousHash}, hash: ${this.block.hash}, data: ${this.block.data}, difficulty: ${this.block.difficulty} ]`;
    }
}
