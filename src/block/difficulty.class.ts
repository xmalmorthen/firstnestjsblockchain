import { BlockInterface } from "./block.interface";

export class DifficultyClass {

    private static MINE_RATE = 3000;

    public static calculate(previousBlock: BlockInterface, timestamp: number): number {
        const { difficulty } = previousBlock;
        return previousBlock.timestamp + this.MINE_RATE > timestamp ? difficulty + 1 : difficulty -1;
    }
}
