import { SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { BlockChainService } from './blockChain/block-chain.service';
import { P2PMESSAGETYPE } from './enumerations/p2pMessageType.enum';

@WebSocketGateway( +process.env.WS || 5000, 
  { 
    //path: 'blockChain',
    //pingInterval: 5000, 
    //pingTimeout: 5000 
  })
export class AppGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  
  @WebSocketServer()
  server: Server;

  private logger: Logger = new Logger('AppGateway');

  constructor( private readonly blockchain: BlockChainService ) {}

  afterInit(server: any) {
    this.logger.log(`WebSocket initialized in port [ ${+process.env.WS || 5000} ]`);
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }
  
  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`Client connected: ${client.id}`);
  }

  @SubscribeMessage('blocks') 
  handleMessage(client: Socket, data: any): void{
    
    if (data.type === P2PMESSAGETYPE.BLOCKS)
      this.blockchain.replace(data.value.blocks);
      
  }

}
