import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { BlockChainService } from './blockChain/block-chain.service';
import { AppGateway } from './app.gateway';
import { P2pService } from './p2p/p2p.service';

@Module({
  imports: [  ],
  controllers: [AppController],
  providers: [ BlockChainService, AppGateway, P2pService ],
})
export class AppModule {}
