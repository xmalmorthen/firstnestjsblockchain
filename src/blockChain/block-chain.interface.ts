import { BlockInterface } from "src/block/block.interface";

export interface BlockChainBase {
    blockChainLength: number
}

export interface BlockChainBlocksResponse extends BlockChainBase {
    blocks: BlockInterface[]
}

export interface BlockChainMineResponseInterface extends BlockChainBase {
    block: BlockInterface
}
