import { json } from 'express';
import { BlockClass } from 'src/block/block.class';
import { BlockInterface } from 'src/block/block.interface';

export class BlockChainClass {
    
    blocks: BlockInterface[] = [];

    constructor(){
        this.blocks = [ BlockClass.genesis.block ];
    }

    add(data: string): BlockInterface{
        const previousBlock: BlockInterface = this.blocks[ this.blocks.length -1 ]
        const blockInstance: BlockClass = BlockClass.mine( previousBlock, data );

        this.blocks.push( blockInstance.block );

        return blockInstance.block;
    }

    private __validate(blockChain: BlockInterface[] = []): boolean {
        const [ genesisBlock, ...blocks ] = blockChain.length ? blockChain : this.blocks;

        if ( JSON.stringify(genesisBlock) !== JSON.stringify( BlockClass.genesis.block ) )
            throw new Error("Invalid Genesis block");
            
        let iter: number = 0;
        blocks.forEach( (item: BlockInterface) => {
            
            const previousBlock: BlockInterface =  blockChain[iter];
            iter++;

            if ( item.previousHash !== previousBlock.hash ){
                console.log(item.previousHash, previousBlock.hash)
                throw new Error("Invalid previous hash");
            }
            if ( item.hash !== BlockClass.hash( item.timestamp, item.nonce, item.previousHash, item.data, item.difficulty ) )
                throw new Error("Invalid hash");

        });

        return true;
    }

    get validate(): boolean{
        return this.__validate();
    }

    replace( newBlocks: BlockInterface[] = [] ){

        if ( newBlocks.length < this.blocks.length )
            throw new Error("Received chain is not longer than current chain");
            
        try {
            this.__validate(newBlocks);
        } catch (error) {
            throw new Error("Received chain is invalid");
        }
        
        this.blocks = newBlocks;

        return this.blocks;
    }
}
