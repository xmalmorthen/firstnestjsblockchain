import { Injectable } from '@nestjs/common';
import { BlockInterface } from 'src/block/block.interface';
import { BlockChainClass } from './block-chain-class';
import { BlockChainBlocksResponse, BlockChainMineResponseInterface } from './block-chain.interface';

@Injectable()
export class BlockChainService {
  private blockchain: BlockChainClass = new BlockChainClass();

  constructor(){
    this.blockchain = new BlockChainClass();
  }

  public add(data: any): BlockChainMineResponseInterface{

    const block: BlockInterface = this.blockchain.add(data);
    const response: BlockChainMineResponseInterface = {
      blockChainLength: this.blockchain.blocks.length,
      block
    }
    return response;

  }

  public replace(blocks: BlockInterface[]){
    try {
      this.blockchain.replace(blocks)
    } catch (error) {}
  }

  get blocks(): BlockChainBlocksResponse{

    const response: BlockChainBlocksResponse = {
      blockChainLength : this.blockchain.blocks.length,
      blocks : this.blockchain.blocks
    }

    return response;
  }

  get validate(): boolean{

    try {
      return this.blockchain.validate;
    } catch (error) {
      return false;
    }

  }
}
