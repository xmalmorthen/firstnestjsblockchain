import { Injectable } from '@nestjs/common';
import { BlockChainService } from 'src/blockChain/block-chain.service';
import { P2PMESSAGETYPE } from 'src/enumerations/p2pMessageType.enum';
import * as io from 'socket.io-client';
import { p2pMessageResponse } from './p2p.interface';


@Injectable()
export class P2pService {

    public peers: string[] = process.env.PEERS ? process.env.PEERS.split(',') : [];
    public sockets: SocketIOClient.Socket[] = [];

    constructor( private readonly blockchain: BlockChainService ){
        this.peers.forEach(peer => {
            this.socketConnect(peer);
        });
    }

    private socketConnect(peer: string): void {
        const socket: SocketIOClient.Socket = io(peer);

        socket.on('connect_error', err => this.handleErrors(err));
        socket.on('connect_failed', err => this.handleErrors(err));
        socket.on('disconnect', err => this.handleErrors(err));

        socket.on( 'blocks', (data: any) => {
            this.onMsg(data);
        });

        this.socketOpen(socket);
    }

    private handleErrors(err:any){
        console.log(err);
    }

    private socketOpen(socket: SocketIOClient.Socket){
        this.sockets.push(socket);

        const msg: p2pMessageResponse = {
            type: P2PMESSAGETYPE.BLOCKS,
            value: this.blockchain.blocks
        };

        socket.emit('blocks',msg);

    }

    private onMsg(data: p2pMessageResponse){
        if (data.type === P2PMESSAGETYPE.BLOCKS)
            this.blockchain.replace(data.value.blocks);
    }

    public broadcast( data: p2pMessageResponse){
        this.sockets.forEach( (socket: SocketIOClient.Socket ) => {
            socket.emit('blocks',data);
        });
    }

    public sync(){
        const msg: p2pMessageResponse = {
            type: P2PMESSAGETYPE.BLOCKS,
            value: this.blockchain.blocks
        };
        this.broadcast(msg);
    }

}
