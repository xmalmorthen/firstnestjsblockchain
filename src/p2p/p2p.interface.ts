import { BlockChainBlocksResponse } from "src/blockChain/block-chain.interface";
import { P2PMESSAGETYPE } from "src/enumerations/p2pMessageType.enum";

export interface p2pMessageBase {
    type: P2PMESSAGETYPE,
    value: BlockChainBlocksResponse
}

export interface p2pMessageResponse extends p2pMessageBase {    
}